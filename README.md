# koa2-oauth-server


## prepare
  ```sh
    npm config set registry="https://registry.npm.taobao.org/"
    npm config set disturl="npm.taobao.org/dist/"
    git clone https://git.oschina.net/cwfan/koa2-oauth-server.git
    cd koa2-oauth-server
    npm install
    npm run dev
  ```
## routes
  ```
  see ./app/routes/
  ```