
const env = process.env.NODE_ENV || 'development'
export default Object.assign({
    port: 3001,
}, require(`./${env}`))


