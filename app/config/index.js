import env from './env'

const config = Object.assign({}, env)
export default Object.assign({}, config, env)