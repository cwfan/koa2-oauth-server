

// generateAccessToken() is optional and should return a _String.
export async function generateAccessToken() {

}
// generateAuthorizationCode() is optional and should return a _String.
export async function generateAuthorizationCode() {

}
// generateRefreshToken() is optional and should return a _String.
export async function generateRefreshToken() {

}

// getAccessToken(token) should return an object with:
// accessToken (String)
// accessTokenExpiresAt (Date)
// client (Object)
// scope (optional String)
// user (Object)
export async function getAccessToken() {

}

// getAuthCode() was renamed to getAuthorizationCode(code) and should return:
// client (Object)
// expiresAt (Date)
// redirectUri (optional String)
// user (Object)
export async function getAuthCode() {

}

// getClient(clientId) should return an object with:
// accessTokenLifetime (optional Integer)
// redirectUris (Array)
// refreshTokenLifetime (optional Integer)
// grants (Array)
export async function getClient(clientId) {

}

// getRefreshToken(token) should return an object with:
// refreshToken (String)
// client (Object)
// refreshTokenExpiresAt (optional Date)
// scope (optional String)
// user (Object)

export async function getRefreshToken(token) {

}

// getUser(username, password) should return an object:
// No longer requires that id be returned.
export async function getUser(username, password) {

}

// getUserFromClient(client) should return an object:
// No longer requires that id be returned.
export async function getUserFromClient(client) {

}

// revokeAuthorizationCode(code) is required and should return:

// expiresAt (Date)
export async function revokeAuthorizationCode(code) {

}
// revokeToken(token) is required and should return:

// refreshTokenExpiresAt (Date)
export async function revokeToken(token) {

}
// saveAccessToken() was renamed to saveToken(token, client, user) and should return:

// accessToken (String)
// accessTokenExpiresAt (Date)
// client (Object)
// refreshToken (optional String)
// refreshTokenExpiresAt (optional Date)
// user (Object)
export async function saveAccessToken() {

}
// saveAuthCode() was renamed to saveAuthorizationCode(code, client, user) and should return:

// authorizationCode (String)
export async function saveAuthorizationCode() {

}
// validateScope(token, scope) should return a Boolean.
export async function validateScope(token, scope) {

}