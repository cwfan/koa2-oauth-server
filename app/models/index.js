import { database } from '../config'
import Sequelize from 'sequelize'
import { users, clients, tokens } from 'Oauth'

let sequelize = new Sequelize(database, {
    define: {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        freezeTableName: true
    }
});

const User = users(sequelize)
const Client = clients(sequelize)
const Token = tokens(sequelize)
export { User, Token, Client }


