import Router from 'koa-router';
import OAuthServer from '../middleware/oauth2'
const router = new Router()
const oauth2 = new OAuthServer()

router.get('/', (ctx) => {
    ctx.body = "Hello word!"
})
router.use('/oauth', oauth2.authenticate())

router.get('/oauth/sign_in',  (ctx) => {
    ctx.body = "Hello authorize!"
})
router.post('/token', oauth2.token())
router.get('/authorize', oauth2.authorize(), (ctx) => {
    ctx.body = "Hello authorize!"
})
export default router